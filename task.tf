variable private_key_path {
}

variable server_extra_disks_count {
default = "2"
}

variable zoneid {
}

variable accountid {
}


variable pub_key_path {
}

variable "countvm" {
  default = "1"
}
variable "indx" {
 default = "0"
}

resource "google_compute_disk" "extra_disk" {
  count = "${var.server_extra_disks_count}"
  name  = "${lookup(local.server_extra_disks[count.index], "name")}"
  size  = "${lookup(local.server_extra_disks[count.index], "size")}"
}

resource "google_compute_attached_disk" "extra_disks_attachments" {
  count       = "${var.server_extra_disks_count}"
  disk        = "${element(google_compute_disk.extra_disk.*.self_link, count.index)}"
  instance    = "${element(google_compute_instance.instance.*.self_link, 0)}"
  device_name = "${lookup(local.server_extra_disks[count.index], "name")}"
}

resource "google_compute_address" "junstaticip" {
name = "junextip"
}

resource "cloudflare_record" "a1exeei" {
zone_id = "${var.zoneid}"
name = "a1exeei.juneway.pro"
value = google_compute_address.junstaticip.address
type = "A"
ttl = 3600
}

data "google_compute_image" "debianimage" {
family = "debian-10"
project = "debian-cloud"
}


resource "google_compute_instance"  "instance"  {
  name = "node1"
  machine_type = "e2-small"
  
  connection {
   user = "yanaleshenko3108"
   private_key = "${file(var.private_key_path)}"
   timeout = "2m"
   host = "${self.network_interface.0.access_config.0.nat_ip}"
  }
  provisioner "remote-exec" {
    inline = [
    "sudo apt update",
    "sudo apt install -y -y ca-certificates apt-transport-https curl gnupg2 lsb-release software-properties-common",
    "sudo curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -",
    "sudo add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/debian  '$(lsb_release -cs)'  stable'",
    "sudo apt update",
    "apt-cache policy docker-ce",
    "sudo apt-get install docker-ce -y",
    "sudo systemctl start docker",
    "sudo groupadd docker",
    "sudo usermod -aG docker yanaleshenko3108",
    "sudo docker pull nginx:latest",
    "echo ''Juneway' ${self.network_interface.0.network_ip} ' ' ${self.name}'| sudo tee /tmp/index.nginx-debian.html",
    "sudo docker run -d -p 80:80  -v /tmp/index.nginx-debian.html:/usr/share/nginx/html/index.html nginx"
    ]   
  }
  
  boot_disk {
    initialize_params  {
     image = "${data.google_compute_image.debianimage.self_link}"
     }
    }
  network_interface {
     network = "default"
     access_config {
       nat_ip ="${google_compute_address.junstaticip.address}"
    }
   }
  lifecycle {
    ignore_changes = [attached_disk]
  }
 
}



resource "google_compute_firewall" "http" {
  name = "tcpall1"
  network = "default"
dynamic  "allow" {
     for_each = ["80","8001","8802"]
      content {
        ports = [allow.value]
        protocol = "tcp" 
     }
    }
  source_ranges = ["0.0.0.0/0"]
}



